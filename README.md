
# Usage overview of `unlock-device-encryption-quick-hack.sh`

Original upstream README.md was moved to [README.upstream.md](README.upstream.md), this file only describes how the wrapper shell script works.

The following binaries are required by the script: `python`, `inotifywait`, `sg_raw`.

`unlock-device-encryption-quick-hack.sh` is an interactive command line tool to guide users through the process of unlocking an encrypted Western Digital storage device.

* At the beginning, the physical block storage device should not yet be connected to the system
* The first prompt just waits to get acknowledged by &lt;Return&gt;
* Connect the physical block storage device and wait for SCSI device names to appear on the output
* Copy the last SCSI generic device name (/dev/sg?) to the prompt that asks for it
* Copy the last SCSI disk device name (/dev/sd?) to the prompt that asks for it
* Observe the SCSI disk device size and partition information prior to unlocking (partitions are not present as the disklabel cannot be read from a locked storage device)
* Type in the password to unlock the storage device when it is prompted for (it is hashed by the python script and written to a named pipe for `sg_raw` to read)
* Acknowledge the prompt by pressing &lt;Return&gt;
* `sudo` is needed, as sending a raw SCSI command requires elevated privileges
* The disklabel of the SCSI disk device is read again by the kernel
* The resulting SCSI disk device details are shown (now with partitions also present if unlocking succeeded)
