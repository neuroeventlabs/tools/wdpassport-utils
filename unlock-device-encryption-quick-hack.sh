#!/bin/sh

# Quick hack to guided unlock of device hardware encryption
# Paavo Hartikainen <paavo.hartikainen@neuroeventlabs.com>

set -o nounset

finished="false"
interrupt="false"

exit_callback() {
  if ! "${finished}" ; then
    echo "Script execution failed"
  fi
  echo "Removing \"${tmppipe}\""
  rm "${tmppipe}"
  rm --dir "${tmpfolder}"
  stty echo
}

trap exit_callback EXIT

interrupt_callback() {
  echo "Script execution interrupted"
  echo "Interrupt again to quit"
  if "${interrupt}" ; then
    exit 0
  else
    interrupt="true"
  fi
}

trap interrupt_callback INT

hangup_callback() {
  echo "Hangup caught"
}

trap hangup_callback HUP

# Create named pipe for providing hashed secret for unlocking

tmpfolder="$(mktemp --directory)"
tmppipe="${tmpfolder}/secret"
echo "Creating \"${tmppipe}\""
mknod "${tmppipe}" p

cat <<EOF
This tool unlocks "Western Digital My Passport" hardware encryption
Make sure block storage device is not attached to system yet.

This tool requires "python", "inotifywait" and "sg_raw".

EOF

read -p "Press <Return> to proceed" response

cat <<EOF
Now connect block storage device to system within 30 seconds.
You should see device names appear when it shows up on bus.
SCSI generic device ("/dev/sg?") and SCSI disk device ("/dev/sd?")
are required by this tool. Latest device names on list should be
correct.

EOF

( sleep 30 && pkill -HUP inotifywait ) &

inotifywait --format '%w%f' --event CREATE --monitor /dev/

cat <<EOF
Which SCSI generic device would you like to unlock?

EOF

read -p "SCSI generic device name: " generic_device

cat <<EOF
Which SCSI disk device would you like to unlock?

EOF

read -p "SCSI disk device name: " disk_device

# Test that SCSI devices exist and are of appropriate type,
# otherwise it is useless to continue

test ! -e "${generic_device}" && echo "SCSI generic device does not exist" && exit 0
test ! -e "${disk_device}" && echo "SCSI disk device does not exist" && exit 0
test ! -c "${generic_device}" && echo "SCSI generic device should be character special file" && exit 0
test ! -b "${disk_device}" && echo "SCSI disk device should be block special file" && exit 0

echo "Proceeding with given input"

echo "Trying to unlock \"${disk_device}\" through \"${generic_device}\""

stty -echo
read -p "Enter password for unlocking: " password
stty echo

./cookpw.py "${password}" | (tee "${tmppipe}" | wc --bytes ; echo "Line above counts bytes in hashed secret, it should be 40") &

echo "Disk device \"${disk_device}\" before unlocking:"
echo "---"
lsblk "${disk_device}"
echo "---"

echo "Now it is time to send following raw SCSI command:"
echo "sudo sg_raw --send=40 --infile=\"${tmppipe}\" \"${generic_device}\" c1 e1 00 00 00 00 00 00 28 00"
read -p "Interrupt now or proceed to sudo with <Return>" response

sudo sg_raw --send=40 --infile="${tmppipe}" "${generic_device}" c1 e1 00 00 00 00 00 00 28 00

echo "Trying to rescan \"${disk_device}\""

sudo partprobe "${disk_device}"

echo "Disk device \"${disk_device}\" after unlocking:"
echo "---"
lsblk "${disk_device}"
echo "---"

finished="true"
